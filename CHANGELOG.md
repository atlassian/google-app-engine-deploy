# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.7.2

- patch: Internal maintenance: Bump base docker image to 513.0.0-slim.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.7.1

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to google/cloud-sdk:504.0.1

## 1.7.0

- minor: Update pipe's base docker image to google/cloud-sdk:487.0.0.
- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.
- patch: Internal maintenance: bump project's dependencies to the latest.

## 1.6.0

- minor: Update cloud-sdk docker image to 473.0.0.
- patch: Internal maintenance: Update test app to node18.
- patch: Update examples in the Readme.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 1.2.0

- minor: Bump goolge cloud sdk docker image from slim to 434.0.0 version to cover needs for Java deploy.
- patch: Bump default image and pipes versions in pipelines release config file.
- patch: Bump test/requirements package versions.

## 1.1.1

- patch: Change cloud-sdk image to slim to reduce size.

## 1.1.0

- minor: Update cloud-sdk docker image to 402.0.0.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 1.0.0

- major: Bump google/cloud-sdk version to 347.0.0.

## 0.7.4

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.7.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.7.2

- patch: Update README: clear description for base64 encoded environment variables

## 0.7.1

- patch: Internal maintenance: Add gitignore secrets.
- patch: Internal maintenance: Update pipe release process.

## 0.7.0

- minor: Bump google/cloud-sdk version to 286.0.0.

## 0.6.1

- patch: Update the Readme with a new Atlassian Community link.

## 0.6.0

- minor: Bump google/cloud-sdk version to 273.0.0

## 0.5.1

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.5.0

- minor: Added a new parameter to set the cloud build timeout

## 0.4.1

- patch: Internal maintenance: Update Readme with Java examples.

## 0.4.0

- minor: Internal maintenance: use large docker image google-cloud-sdk
- patch: Add parameter --quiet to main deploy command and prerequisites section to Readme

## 0.3.1

- patch: Switched to google/cloud-sdk:242.0.0-alpine image

## 0.3.0

- minor: Allow passing multiple deployment configs (deployables)

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming conventions from task to pipes.

## 0.1.3

- patch: Use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Remove details.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines GCP app engine deploy pipe.
