# "upload to GCP app engine"
# "upload to GCP app engine different deployable"
import os
import random
from string import Template

import requests
from bitbucket_pipes_toolkit.test import PipeTestCase


class GAEDeployTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()

        self.random_number = random.randrange(0, 32767)

        # update app template
        with open("./test/app.js.template", "r") as template:
            data = template.read()
            app_config = Template(data).substitute(BODY=self.random_number)

        with open("./test/app.js", "w") as template:
            template.write(app_config)

        # update package template
        with open("./test/package.json.template", "r") as template:
            data = template.read()
            package_config = Template(data).substitute(BODY=self.random_number)

        with open("./test/package.json", "w") as template:
            template.write(package_config)

    def tearDown(self):
        # clean up tests resources
        if os.path.exists("./test/app.js"):
            os.remove("./test/app.js")
        if os.path.exists("./test/package.json"):
            os.remove("./test/package.json")

    def test_upload_to_GCP_app_engine(self):
        result = self.run_container(
            environment={
                'KEY_FILE': os.getenv('GCP_KEY_FILE'),
                'PROJECT': os.getenv('PROJECT'),
                'VERSION': os.getenv('BITBUCKET_BUILD_NUMBER'),
                'PROMOTE': "true",
                'STOP_PREVIOUS_VERSION': "true",
            },
            working_dir=os.path.join(os.getcwd(), "test")
        )

        self.assertIn('Deployment successful', result)

        response = requests.get(
            f"https://service-0-dot-{os.getenv('PROJECT')}.appspot.com",
            headers={'Cache-Control': 'no-cache'},
        )
        self.assertEqual(response.status_code, requests.codes.OK)
        self.assertEqual(response.text, str(self.random_number))

    def test_upload_to_GCP_app_engine_different_deployable(self):
        result = self.run_container(
            environment={
                'KEY_FILE': os.getenv('GCP_KEY_FILE'),
                'PROJECT': os.getenv('PROJECT'),
                'DEPLOYABLES': "app-1.yaml",
                'VERSION': os.getenv('BITBUCKET_BUILD_NUMBER'),
                'PROMOTE': "true",
                'STOP_PREVIOUS_VERSION': "true",
            },
            working_dir=os.path.join(os.getcwd(), "test")
        )

        self.assertIn('Deployment successful', result)

        response = requests.get(
            f"https://service-1-dot-{os.getenv('PROJECT')}.appspot.com",
            headers={'Cache-Control': 'no-cache'},
        )
        self.assertEqual(response.status_code, requests.codes.OK)
        self.assertEqual(response.text, str(self.random_number))

    def test_upload_to_GCP_app_engine_small_timeout(self):
        result = self.run_container(
            environment={
                'KEY_FILE': os.getenv('GCP_KEY_FILE'),
                'PROJECT': os.getenv('PROJECT'),
                'DEPLOYABLES': "app-2.yaml",
                'VERSION': f"{os.getenv('BITBUCKET_BUILD_NUMBER')}-1",
                'PROMOTE': "true",
                'STOP_PREVIOUS_VERSION': "true",
                'CLOUD_BUILD_TIMEOUT': 1,
            },
            working_dir=os.path.join(os.getcwd(), "test")
        )

        self.assertIn('Deployment failed', result)
        self.assertIn('Cloud build did not succeed', result)
