FROM google/cloud-sdk:513.0.0-slim

WORKDIR /

# copy the pipe source code
COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["/pipe.sh"]
